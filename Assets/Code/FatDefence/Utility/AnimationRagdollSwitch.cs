﻿using UnityEngine;

namespace Code.FatDefence.Utility
{
    [DisallowMultipleComponent]
    public class AnimationRagdollSwitch : MonoBehaviour
    {
        private Rigidbody[] _rigidbodies;
        private Animator _animator;
        
        private void Awake()
        {
            _rigidbodies = GetComponentsInChildren<Rigidbody>();
            _animator = GetComponentInChildren<Animator>();
            
            DeactivateRagdoll();
        }

        public AnimationRagdollSwitch ActivateRagdoll() => SwitchRagdollStateTo(true);

        public AnimationRagdollSwitch DeactivateRagdoll() => SwitchRagdollStateTo(false);

        // This method probably should not be here. Or maybe it should.
        public void AddForce(Vector3 impulse, ForceMode forceMode)
        {
            foreach (var item in _rigidbodies)
            {
                item.AddForce(impulse, forceMode);
            }
        }
        
        private AnimationRagdollSwitch SwitchRagdollStateTo(bool newState)
        {
            _animator.enabled = !newState;
            foreach (var item in _rigidbodies)
            {
                item.isKinematic = !newState;
            }

            return this;
        }
    }
}
﻿namespace Code.FatDefence.Utility
{
    public static class Layers
    {
        public const int CollectiblesLayer = 6;
        public const int GroundLayer = 7;
        public const int EnemyLayer = 8;
    }
}
﻿using Code.FatDefence.Hero;
using Cysharp.Threading.Tasks;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Code.FatDefence
{
    public class TimeScaleChanger : MonoBehaviour
    {
        [SerializeField] private float timeScale = 2f;
        [SerializeField] private float exitDuration = 1f;

        private void Awake()
        {
            Bind();
        }

        private void Bind()
        {
            var col = GetComponent<Collider>();
            col.isTrigger = true;

            Time.fixedDeltaTime = 0.02f;
            
            col.OnTriggerEnterAsObservable()
                .Select(col => col.GetComponentInParent<PhysicsBasedMovement>())
                .Where(col => col)
                .Take(1)
                .Subscribe(_ =>
                {
                    Time.timeScale = timeScale;
                    Time.fixedDeltaTime *= Time.timeScale;
                })
                .AddTo(this);

            col.OnTriggerExitAsObservable()
                .Select(col => col.GetComponentInParent<PhysicsBasedMovement>())
                .Where(col => col)
                .Take(1)
                .Subscribe(_ => Do())
                .AddTo(this);
        }

        private async void Do()
        {
            print("exiting");
            var time = exitDuration;
            while (time > 0f)
            {
                time -= Time.deltaTime;
                Time.timeScale -= Time.deltaTime;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                await UniTask.Yield();
            }
            Time.timeScale = 1f;
            Time.fixedDeltaTime = Time.fixedUnscaledDeltaTime;
        }
    }
}
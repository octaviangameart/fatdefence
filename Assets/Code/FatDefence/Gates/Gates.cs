﻿using System;
using TMPro;
using UnityEngine;

namespace Code.FatDefence.Gates
{
    [DisallowMultipleComponent]
    public class Gates : MonoBehaviour
    {
        [SerializeField] private int value = 1;
        [SerializeField] private OperationType operationType = OperationType.Addition;

        private void Awake()
        {
            var label = GetComponentInChildren<TextMeshPro>();
            label.text = $"{GetOperationSigh(operationType)}{value}";
        }

        public int GetOperationResult(int argument)
        {
            var rawResult = GetOperationResult(operationType, argument);
            return Mathf.Clamp(rawResult, 0, 6);
        }
        
        private int GetOperationResult(OperationType operationType, int argument) => operationType switch
        {
            OperationType.Addition => argument + value,
            OperationType.Subtraction => argument - value,
            OperationType.Multiplication => argument * value,
            OperationType.Division => argument / value,
            _ => throw new ArgumentOutOfRangeException(nameof(operationType), operationType, null)
        };

        private char GetOperationSigh(OperationType operationType) => operationType switch
        {
            OperationType.Addition => '+',
            OperationType.Subtraction => '-',
            OperationType.Multiplication => '*',
            OperationType.Division => '/',
            _ => throw new ArgumentOutOfRangeException(nameof(operationType), operationType, null)
        };
    }
}
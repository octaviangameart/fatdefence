﻿namespace Code.FatDefence.Gates
{
    public enum OperationType
    {
        Addition = 0,
        Subtraction = 1,
        Multiplication = 2,
        Division = 3
    }
}
﻿using Code.FatDefence.Enemies;
using Code.FatDefence.Utility;
using Octavian.Runtime.CollisionHandling;
using UniRx;
using UnityEngine;

namespace Code.FatDefence.Hero
{
    public class CollisionDetectionInitializer : MonoBehaviour
    {
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private void Awake()
        {
            Bind();
        }

        private void Bind()
        {
            var heroCollider = GetComponentInChildren<Collider>();
            var fatmanSpawner = GetComponentInChildren<FatmanSpawner>();
            var gatesDetector = new ComponentBasedCollisionDetector<Gates.Gates>(heroCollider, Layers.CollectiblesLayer).AddTo(_disposable);
            var groundDetector = new LayerBasedCollisionDetector(heroCollider, Layers.GroundLayer).AddTo(_disposable);
            var enemyDetector = new ComponentBasedCollisionDetector<EnemyMarker>(heroCollider, 
                    ComponentSelector.AllHierarchy<EnemyMarker>, Layers.EnemyLayer)
                .AddTo(_disposable);
            
            Observable.FromEvent<Gates.Gates>(h => gatesDetector.Collided += h,
                    h => gatesDetector.Collided -= h)
                .Subscribe(gates =>
                {
                    var newVisibleFatmanQuantity = gates.GetOperationResult(fatmanSpawner.CurrentIndex);
                    fatmanSpawner.ChangeVisibleFatmanQuantity(newVisibleFatmanQuantity);
                    Destroy(gates.gameObject);
                })
                .AddTo(_disposable);
            
            Observable.FromEvent<CollisionData>(h => groundDetector.ChangedCollisionPhase += h,
                    h => groundDetector.ChangedCollisionPhase -= h)
                .Skip(1)
                .Where(data => data.CollisionPhase == CollisionPhase.Enter)
                .Subscribe(data => EndGameCollision(heroCollider.attachedRigidbody, fatmanSpawner))
                .AddTo(_disposable);
            
            Observable.FromEvent<EnemyMarker>(h => enemyDetector.Collided += h,
                    h => enemyDetector.Collided -= h)
                .Subscribe(@switch => EndGameCollision(heroCollider.attachedRigidbody, fatmanSpawner))
                .AddTo(_disposable);
        }

        private void EndGameCollision(Rigidbody rigidbody, FatmanSpawner fatmanSpawner)
        {
            rigidbody.constraints = RigidbodyConstraints.None;
            fatmanSpawner.ActivateAll(rigidbody.velocity);
        }

        private void OnDestroy()
        {
            _disposable.Dispose();
        }
    }
}
﻿using UnityEngine;

namespace Code.FatDefence.Hero
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody))]
    public class Fatman : MonoBehaviour
    {
        [SerializeField] private GameObject wings;
        
        private Rigidbody _rigidbody;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            ChangeWingsVisibility(false);
        }

        public void ChangeWingsVisibility(bool newState)
        {
            wings.SetActive(newState);
        }

        public void ActivatePhysics(Vector3 heroVelocity)
        {
            transform.parent = null;
            _rigidbody.isKinematic = false;
            _rigidbody.useGravity = true;
            _rigidbody.velocity = heroVelocity + Random.insideUnitSphere * 15f;
            _rigidbody.drag = 0.2f;
        }
    }
}
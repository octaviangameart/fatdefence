﻿using Cysharp.Threading.Tasks;
using NaughtyAttributes;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Code.FatDefence.Hero
{
    [DisallowMultipleComponent]
    public class PhysicsBasedMovement : MonoBehaviour
    {
        public bool isChangingTimeScale = false;
        [SerializeField] private float startingTimeScale = 2f;
        [SerializeField] private Transform target;
        [SerializeField] private float angle;
        [SerializeField] private float glidingSpeed;
        [SerializeField] private float transitionToGlidingSpeedDuration;
        [SerializeField] private float fallEndGlideAcceleration;
        [SerializeField] private float slideSensitivity;

        [Foldout("References:")] [SerializeField] private GameObject wings;

        private Rigidbody _rigidbody;
        private Vector3 _previousMousePosition;

        private void Awake()
        {
            wings.SetActive(false);
            
            _rigidbody = GetComponent<Rigidbody>();
            var fatmanSpawner = GetComponent<FatmanSpawner>();
            var isControlsEnabled = false;
            var isGliding = false;
            //
            // if (isChangingTimeScale)
            // {
            //     Time.timeScale = startingTimeScale;
            //     Time.fixedDeltaTime *= Time.timeScale;
            // }

            this.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonDown(0))
                .Take(1)
                .Subscribe(_ =>
                {
                    this.UpdateAsObservable()
                        .TakeWhile(_ => _rigidbody.velocity.y >= 0)
                        .Subscribe(_ => {}, () =>
                        {
                            Debug.Log("enabling controlls.");
                            _previousMousePosition = Input.mousePosition;
                            isControlsEnabled = true;

                            // if (isChangingTimeScale)
                            // {
                            //     Time.timeScale = 1f;
                            //     Time.fixedDeltaTime = Time.fixedUnscaledDeltaTime;
                            // }
                        })
                        .AddTo(this);
                    
                    StartMovement();
                })
                .AddTo(this);

            this.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonDown(0))
                .Skip(1)
                .Where(_ => isControlsEnabled)
                .Subscribe(_ =>
                {
                    isGliding = true;
                    wings.SetActive(true);
                    fatmanSpawner.ChangeWingsVisibility(true);
                    _rigidbody.useGravity = false;
                    _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, -glidingSpeed, _rigidbody.velocity.z);

                    _previousMousePosition = Input.mousePosition;
                })
                .AddTo(this); 
            
            this.UpdateAsObservable()
                .Where(_ => Input.GetMouseButton(0))
                .Where(_ => isControlsEnabled)
                .Select(_ => GetMouseDelta())
                .Subscribe(delta =>
                {
                    delta *= -1f;
                    var newVelocity = _rigidbody.velocity;
                    newVelocity.x = delta * slideSensitivity * Time.fixedDeltaTime;
                    _rigidbody.velocity = newVelocity;
                })
                .AddTo(this); 

            this.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonUp(0))
                .Skip(1)
                .Where(_ => isControlsEnabled)
                .Where(_ => isGliding)
                .Subscribe(_ =>
                {
                    _rigidbody.AddForce(Vector3.down * fallEndGlideAcceleration, ForceMode.Impulse);
                    _rigidbody.useGravity = true;
                    isGliding = false;
                    wings.SetActive(false);
                    fatmanSpawner.ChangeWingsVisibility(false);
                })
                .AddTo(this);
        }

        private void StartMovement()
        {
            _rigidbody.velocity = CalculateStartingVelocity(target, angle);
        }
        
        private Vector3 CalculateStartingVelocity(Transform target, float angle) 
        {
            var direction = target.position - transform.position;
            direction.y = 0;
            var distance = direction.magnitude ;  
            var angleRad = angle * Mathf.Deg2Rad;  
            direction.y = distance * Mathf.Tan(angleRad); 
            var velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * angleRad));
            return velocity * direction.normalized;
        }

        private float GetMouseDelta()
        {
            var fullDelta = _previousMousePosition - Input.mousePosition;
            return fullDelta.x;
        }
    }
}
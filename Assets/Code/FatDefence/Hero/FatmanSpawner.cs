﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.FatDefence.Hero
{
    public class FatmanSpawner : MonoBehaviour
    {
        public int CurrentIndex { get; private set; } = 0;
        
        private List<Fatman> _fatmen = new List<Fatman>();

        private void Awake()
        {
            _fatmen = GetComponentsInChildren<Fatman>().ToList();
            _fatmen.ForEach(f => f.gameObject.SetActive(false));
        }

        public void ChangeWingsVisibility(bool newState)
        {
            _fatmen.ForEach(f => f.ChangeWingsVisibility(newState));
        }

        public void ChangeVisibleFatmanQuantity(int newValue)
        {
            CurrentIndex = newValue;
            
            _fatmen.ForEach(f => f.gameObject.SetActive(false));
            for (var i = 0; i < newValue; i++)
            {
                _fatmen[i].gameObject.SetActive(true);
            }
        }

        public void ActivateAll(Vector3 heroVelocity) => 
            _fatmen
                .Where(f => f.gameObject.activeInHierarchy)
                .ToList()
                .ForEach(f => f.ActivatePhysics(heroVelocity));
    }
}